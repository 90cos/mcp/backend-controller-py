FROM alpine:3.18
ENV PROTOCOL_BUFFERS_PYTHON_IMPLEMENTATION=python
  
RUN apk update && \
    apk add --no-cache python3 py3-pip supervisor

COPY ./requirements.txt /tmp/requirements.txt

RUN pip3 install --extra-index-url https://gitlab.com/api/v4/projects/25582434/packages/pypi/simple ramrodbrain && \
    pip3 install -r /tmp/requirements.txt

# PATCH for upstream rethink issue
RUN echo "from collections.abc import Callable, Mapping, Iterable" >> /usr/lib/python$(python3 -c "from sys import version_info as v; print(f'{v[0]}.{v[1]}', end='')")/collections/__init__.py


RUN adduser -D ramrod

WORKDIR /opt/app-root/src
COPY . .
COPY supervisord.conf /etc/supervisor/conf.d/supervisord.conf
USER root

CMD [ "/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
