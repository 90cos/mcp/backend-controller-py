import sys
from time import sleep
import controller.startup.advertise as advertise
import controller.startup.locate as locate
import controller.startup.service_startup as srv_start

if __name__ == "__main__":
    sleep(4)
    srv_start.verify_service_status()
    srv_start.verify_expected_active_services()
    srv_start.startup_services()
    advertise.advertise_nodes()
    advertise.advertise_plugins(sys.argv[1])  # legacy plugin import method via manifest.json
    locate.import_modern_plugins()  # modern plugin method via image naming convention
