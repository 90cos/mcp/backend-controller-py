# Backend Plugin Controller

Controller is meant to be a Docker container with 4 majore subsystems running

1. RunOnce(Bootstrapping from manifest)
2. Desired State Enforcer (Reads brain, makes docker API calls)
3. Current state monitor (reads docker events, updates brain)
4. Logs  (collects docker events and servcice logs, passes to brain)



## Architecture
![alt text](https://git.badger.net/PCP/backend-controller-py/raw/master/backend-controller-py-arch.png "PCP/backend-controller-py architecture")

## (Placeholder)