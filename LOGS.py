
from time import sleep
from concurrent.futures import ThreadPoolExecutor
from controller.logs import Logger
import docker
from brain import connect
from os import environ


def service_logging(service):
    logger = Logger(service)
    logger.follow_logs()


def handle_service_create(event):
    client = docker.from_env()
    service = client.services.get(event['Actor']['ID'])
    service_logging(service)


def gather_existing_services(executor):
    client = docker.from_env()
    services = client.services.list()
    for service in services:
        # service_logging(service)  # single threaded function call
        executor.submit(service_logging, service)  # multi threaded function call


if __name__ == "__main__":
    # Throws exception, logger dies if brain not ready (desired for the service to restart)
    brain_ready = connect(host=environ.get("RETHINK_HOST", "localhost"), timeout=2)
    main_client = docker.from_env()
    max_workers = int(environ.get("MAX_WORKERS", 128))
    with ThreadPoolExecutor(max_workers=max_workers) as executor:
        gather_existing_services(executor)
        for event in main_client.events(decode=True):
            if event['Type'] == "service" and event['Action'] == "create":
                # handle_service_create(event)  # single threaded function call (for easier debugging)
                executor.submit(handle_service_create, event)  # multi threaded function call
