from brain import connect
from brain.static import RPC

from controller.csm.current_state_monitor import CurrentStateMonitor

if __name__ == '__main__':
    test = CurrentStateMonitor()
    test.run()
