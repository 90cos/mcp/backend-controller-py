import docker
from os import environ
from copy import deepcopy
from uuid import uuid4
# from time import sleep

from brain import connect
from brain.static import RPC
from controller.dse import enforcer

CLIENT = docker.from_env()

TEST_PLUGIN_DATA = {
    "Name": "Harness",
    "ServiceName": "Harness-7070tcp",
    "ServiceID": "",
    "State": "",
    "DesiredState": "Activate",
    "Interface": "11.22.33.4",
    "OS": "posix",
    "Extra": False,
    "Environment": ["STAGE=DEV", "NORMAL=1"],
    "ExternalPorts": ["7070/tcp"],
    "InternalPorts": ["7070/tcp"]
}


def beta_provisioning_plugin():
    """
    # Test through the whole provision plugin flow in pcp-core

    # Pre-config
    1. docker swarm init
    2. docker network create --driver=overlay --attachable pcp
    3. docker run -d --rm --name rethinkdb -p 28015:28015 -e "STAGE=DEV"
       -e "LOGLEVEL=DEBUG" --network pcp ramrodpcp/database-brain:dev
    4. docker node update --label-add "os=posix" $LEADER_HOST(node's hostname)
    5. docker node update --label-add "ip=1.1.1.1" $LEADER_HOST(node's hostname)

    # Monitor all events down below
    # 1. insert or update RPC with provisioned plugin
         ({"DesiredState": "Activate", "State": ""})
         (mimicking provisioning plugin from the UI)
    # 2. create plugin service, and if docker environment is configured
         properly it will update plugin service automatically.
    # 3. Once service is running and replica is 1/1, csm should update
         RPC by filtering service name and update plugin's service id
    # 4. create plugin container, and then start plugin container
    # 5. update RPC
    :return:
    """

    # 1. insert or update RPC with provisioned plugin
    RPC.insert(TEST_PLUGIN_DATA).run(connect())

    # 2. create plugin service, and then update plugin service
    environ["TAG"] = "dev"
    new_service = deepcopy(TEST_PLUGIN_DATA)
    new_service["Interface"] = "1.1.1.1"

    new_service["ServiceName"] = "Harness-7070tcp"
    new_service["ExternalPorts"] = ["7070/tcp"]
    new_service["InternalPorts"] = ["7070/tcp"]

    # new_service["ServiceName"] = "Harness-7080tcp"
    # new_service["ExternalPorts"] = ["7080/tcp"]
    # new_service["InternalPorts"] = ["7080/tcp"]

    new_service["id"] = str(uuid4())

    out_svc = enforcer.Enforcer().generate_service_create(new_service)
    image_name = enforcer.Enforcer().get_image_from_plugin(new_service)
    CLIENT.services.create(image=image_name, name=new_service["ServiceName"], **out_svc)
    service_list = CLIENT.services.list()
    for service in service_list:
        print("SERVICE NAME: {}".format(service.name))

    # Test # 3
    # Another brain connection to check if service id is updated


if __name__ == '__main__':
    beta_provisioning_plugin()
