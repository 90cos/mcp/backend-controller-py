import docker
import pytest
from os import environ
from uuid import uuid4
from copy import deepcopy
from time import sleep

from brain import connect
from brain.static import RPC
from brain.controller.plugins import find_plugin
from controller.dse import enforcer
from controller.common.controller_common import get_manager_ip
# from controller.csm import current_state_monitor

CLIENT = docker.from_env()
conn = connect()

TEST_PLUGIN_DATA = {
    "Name": "Harness",
    "ServiceName": "Harness-7070tcp",
    "ServiceID": "",
    "State": "",
    "DesiredState": "Activate",
    "Interface": "11.22.33.4",
    "OS": "posix",
    "Extra": False,
    "Environment": ["STAGE=DEV", "NORMAL=1"],
    "ExternalPorts": ["7070/tcp"],
    "InternalPorts": ["7070/tcp"]
}

"""
# Monitor all events down below
# 1. CSM should start running
# 2. insert or update RPC with provisioned plugin
# 3. create plugin service,
#    updating plugin service should be automated,
#    creating and starting a plugin container
#    should be automated as well
# 4. CSM should update plugin's service id within RPC
# 5. CSM should update plugin's Desired State and State within RPC

  Updating test below in the future
# 6. Good health check
# 7. Test bad service data
# 8. Test bad container data
# 9. Test bad health check(unhealthy)
# 10. Test if container status is stopped (exec_die)
# 11. Test if service exist in RPC
"""


# 1. CSM should start running
# @pytest.fixture(scope="function")
# def start_csm():
#     csm_mod = current_state_monitor.CurrentStateMonitor()
#     csm_mod.run()


# 2. insert or update RPC with provisioned plugin
def test_provisioning_plugin():
    insert_plugin = RPC.insert(TEST_PLUGIN_DATA).run(conn)
    assert insert_plugin['inserted'] == 1


# 3. create plugin service
def test_creates_plugin_service():
    environ["TAG"] = "dev"
    new_service = deepcopy(TEST_PLUGIN_DATA)
    new_service["Interface"] = get_manager_ip()
    new_service["ServiceName"] = "Harness-7070tcp"
    new_service["ExternalPorts"] = ["7070/tcp"]
    new_service["InternalPorts"] = ["7070/tcp"]
    new_service["id"] = str(uuid4())

    out_svc = enforcer.Enforcer().generate_service_create(new_service)
    image_name = enforcer.Enforcer().get_image_from_plugin(new_service)
    CLIENT.services.create(image=image_name,
                           # name=new_service["ServiceName"], # fixed issue upstream
                           **out_svc)
    sleep(16)  # slow ci
    assert new_service["ServiceName"] in [x.name for x in CLIENT.services.list()]


# 4. CSM should update plugin's service id within RPC
def test_csm_updated_service_id():
    num_check = 10
    increment_int = 0
    while increment_int < num_check:
        plugins_table = RPC.run(conn)
        for item in plugins_table:
            if item["ServiceID"] != '':
                assert True
        sleep(1)
        increment_int += 1


# 5. CSM should update plugin's Desired State and State within RPC
def test_csm_updated_states():
    num_check = 10
    increment_int = 0
    while increment_int < num_check:
        plugins_table = RPC.run(conn)
        for item in plugins_table:
            if item["DesiredState"] != 'Activate' and item["State"] != '':
                assert True
        sleep(1)
        increment_int += 1


# 6. Good health check
def test_good_health_check():
    assert True


# 7. Test bad service data
def test_bad_service_data():
    assert True


# 8. Test bad container data
def test_bad_container_data():
    assert True


# 9. Test bad health check(unhealthy)
def test_bad_health_check():
    assert True


# 10. Test if container status is stopped (exec_die)
def test_container_status_stopped():
    assert True


# 11. Test if service exist in RPC
def test_service_exist():
    assert find_plugin(TEST_PLUGIN_DATA["ServiceName"], "ServiceName")
