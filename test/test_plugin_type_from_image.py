from controller.common.controller_common import get_plugin_type_from_image
from controller.common import PLUGIN_PREFIX


def test_alpine_37():
    assert get_plugin_type_from_image(f"{PLUGIN_PREFIX}interpreter-plugin:deploy") == ""


def test_ubuntu_18():
    assert get_plugin_type_from_image(f"{PLUGIN_PREFIX}interpreter-plugin-extra:deploy") == "extra"


def test_ubuntu_16():
    assert get_plugin_type_from_image(f"{PLUGIN_PREFIX}interpreter-plugin-ubuntu1604:deploy") == "ubuntu1604"


def test_totally_broken():
    assert get_plugin_type_from_image(f"{PLUGIN_PREFIX}database-brain:dev") == ""


