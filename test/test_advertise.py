from controller.startup import advertise
from copy import deepcopy
from pytest import fixture, raises
from os import environ
from json import JSONDecodeError
from brain.checks import verify
from brain.brain_pb2 import Port
from brain.static import RPP
from brain import connect
import docker


MANIFEST_DATA = [
    {"Name": "TeddyGrams", "OS": "posix", "Extra": True, "ImageName": "extra"},
    {"Name": "ZXShell", "OS": "posix", "Extra": False, "ImageName": ""}
]

EMPTY_PLUGIN = {
        "Name": "",
        "ServiceID": "",
        "ServiceName": "",
        "DesiredState": "",
        "State": "Available",
        "Interface": "",
        "ExternalPorts": [],
        "InternalPorts": [],
        "OS": "",
        "Environment": [],
        "Extra": True,
        "ImageName": ""
    }

PLUGIN_LIST = [

    {
        "Name": "TeddyGrams",
        "ServiceID": "",
        "ServiceName": "",
        "DesiredState": "",
        "State": "Available",
        "Interface": "",
        "ExternalPorts": [],
        "InternalPorts": [],
        "OS": "posix",
        "Environment": [],
        "Extra": True,
        "ImageName": "extra"
    },
    {
        "Name": "ZXShell",
        "ServiceID": "",
        "ServiceName": "",
        "DesiredState": "",
        "State": "Available",
        "Interface": "",
        "ExternalPorts": [],
        "InternalPorts": [],
        "OS": "posix",
        "Environment": [],
        "Extra": False,
        "ImageName": ""
    }
]

CLIENT = docker.from_env()
NODE_INFO = CLIENT.nodes.list()


# @fixture(scope="module")
# def give_brain():
#     # Setup for all module tests
#     tag = environ.get("TRAVIS_BRANCH", "dev").replace("master", "latest")
#     old_stage = environ.get("STAGE", "")
#     environ["STAGE"] = "TESTING"
#     rdb = CLIENT.containers.run(
#         "".join(("ramrodpcp/database-brain:", tag)),
#         name="rethinkdb_rethink",
#         detach=True,
#         ports={"28015/tcp": 28015},
#         remove=True
#     )
#     yield
#     # Teardown for all module tests
#     environ["STAGE"] = old_stage
#     rdb.stop(timeout=5)


def test_map_os_linux():
    assert advertise.map_os("linux") == "posix"
    assert advertise.map_os("posix") == "posix"


def test_map_os_windows():
    assert advertise.map_os("windows") == "nt"
    assert advertise.map_os("nt") == "nt"


def test_map_os_bad():
    assert advertise.map_os(None) is None
    assert advertise.map_os("mac") is None


def test_build_port_from_node():
    node = NODE_INFO[0]
    port = advertise.build_port_from_node(node)
    assert isinstance(port["TCPPorts"], list)
    assert isinstance(port["UDPPorts"], list)


def test_update_node():
    advertise.update_node(NODE_INFO[0])
    assert CLIENT.nodes.list()[0].attrs["Spec"]["Labels"]["ip"] == NODE_INFO[0].attrs["Status"]["Addr"]
    assert CLIENT.nodes.list()[0].attrs["Spec"]["Labels"]["os"] == "posix"


def test_prepare_nodes():
    nodes = advertise.prepare_nodes()
    assert isinstance(nodes, list)
    for node in nodes:
        assert verify(node, Port())


def test_advertise_ip():
    nodes = advertise.prepare_nodes()
    advertise.advertise_ip(nodes)


def test_advertise_node():
    RPP.delete().run(connect())
    assert advertise.advertise_nodes()


def test_get_plugins():
    plugin_list = advertise.get_plugins("test/manifest.json")
    assert isinstance(plugin_list, list)
    assert len(plugin_list) == len(MANIFEST_DATA)
    assert plugin_list == MANIFEST_DATA


def test_get_plugins_default():
    original_file = advertise.MANIFEST_FILE
    advertise.MANIFEST_FILE = "test/manifest.json"
    plugin_list = advertise.get_plugins("test/manifest.json")
    assert isinstance(plugin_list, list)
    assert len(plugin_list) == len(MANIFEST_DATA)
    assert plugin_list == MANIFEST_DATA
    advertise.MANIFEST_FILE = original_file


def test_manifest_not_found():
    with raises(FileNotFoundError):
        advertise.get_plugins("test/notafile.txt")


def test_manifest_empty():
    assert advertise.get_plugins("test/empty.txt") == []


def test_manifest_not_json():
    with raises(JSONDecodeError):
        advertise.get_plugins("test/notjson.txt")


def test_make_plugin():
    made = advertise.make_plugin(MANIFEST_DATA[0])
    plug1 = deepcopy(EMPTY_PLUGIN)
    plug1["Name"] = str(MANIFEST_DATA[0]["Name"])
    plug1["OS"] = str(MANIFEST_DATA[0]["OS"])
    plug1["Extra"] = MANIFEST_DATA[0]["Extra"]
    plug1["ImageName"] = MANIFEST_DATA[0]["ImageName"]
    assert made == plug1

    made2 = advertise.make_plugin(MANIFEST_DATA[1])
    plug2 = deepcopy(EMPTY_PLUGIN)
    plug2["Name"] = MANIFEST_DATA[1]["Name"]
    plug2["OS"] = MANIFEST_DATA[1]["OS"]
    plug2["Extra"] = MANIFEST_DATA[1]["Extra"]
    plug1["ImageName"] = MANIFEST_DATA[1]["ImageName"]
    assert made2 == plug2


def test_prepare_plugins():
    prepped = advertise.prepare_plugins(MANIFEST_DATA)
    assert len(prepped) == len(MANIFEST_DATA)
    assert prepped == PLUGIN_LIST


def test_plugin_advertise():
    assert advertise.plugin_advertise(MANIFEST_DATA)


def test_plugin_advertise_empty():
    assert advertise.plugin_advertise(None) is False


def test_advertise_plugins():
    assert advertise.advertise_plugins("test/manifest.json")
