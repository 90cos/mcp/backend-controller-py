from controller.startup import locate
from controller.common import PLUGIN_PREFIX
import docker
from pytest import fixture


@fixture(scope="module")
def confirm_example_plugins_exist():
    docker.from_env().images.build(
        path=".",
        dockerfile="Dockerfile_LabelExample",
        tag=f"{PLUGIN_PREFIX}interpreter-plugin-labeled:test"
    )
    docker.from_env().images.build(
        path=".",
        dockerfile="Dockerfile_UnlabeledExample",
        tag=f"{PLUGIN_PREFIX}interpreter-plugin-pluginname:test"
    )
    yield True
    docker.from_env().images.remove(f"{PLUGIN_PREFIX}interpreter-plugin-labeled:test")
    docker.from_env().images.remove(f"{PLUGIN_PREFIX}interpreter-plugin-pluginname:test")


def test_get_plugins(confirm_example_plugins_exist):
    located_plugin_list = locate.modern_plugins()
    assert len(located_plugin_list) == 2
    found_label = False
    found_unlabel = False
    for plugin in located_plugin_list:
        if "labeled" in plugin["ImageName"]:
            found_label = True
            assert plugin["Name"] == 'CaSeSEnSiTiVe'
        elif "pluginname" in plugin["ImageName"]:
            found_unlabel = True
            assert plugin["Name"] == 'Pluginname'
    assert found_unlabel
    assert found_label


def test_image_names(confirm_example_plugins_exist):
    result = locate.image_names()
    assert isinstance(result, set)
    assert len(result) == 2
    assert 'pluginname' in result
    assert 'labeled' in result


def test_loaded_to_brain(confirm_example_plugins_exist):
    locate.import_modern_plugins()
    from brain.controller.plugins import get_plugins
    from brain.controller.plugins import get_names
    from brain.controller.plugins import RPC
    from brain import connect
    names = get_names()
    assert "Pluginname" in names
    assert "CaSeSEnSiTiVe" in names

    plugins = get_plugins()
    assert 'pluginname' in [x["ImageName"] for x in plugins if "ImageName" in x]
    assert 'labeled' in [x["ImageName"] for x in plugins if "ImageName" in x]
    RPC.delete().run(connect())


def test_dont_load_same_plugin_twice(confirm_example_plugins_exist):
    from brain import connect
    from brain.static import RPC
    conn = connect()
    a = list(RPC.run(conn))
    assert len(a) == 0
    locate.import_modern_plugins()
    b = list(RPC.run(conn))
    assert len(b) == 2
    locate.import_modern_plugins()
    b = list(RPC.run(conn))
    assert len(b) == 2
    from brain.controller.plugins import get_plugins
    from brain.controller.plugins import get_names
    from brain.controller.plugins import RPC
    from brain import connect
    names = get_names()
    assert "Pluginname" in names
    assert "CaSeSEnSiTiVe" in names

    plugins = get_plugins()
    assert 'pluginname' in [x["ImageName"] for x in plugins if "ImageName" in x]
    assert 'labeled' in [x["ImageName"] for x in plugins if "ImageName" in x]
    RPC.delete().run(connect())


def test_enforcer_correctly_understands_images(confirm_example_plugins_exist):
    from controller.dse.enforcer import Enforcer
    from controller.startup.locate import LEGACY_IMAGE_NAMES
    e = Enforcer()
    assert 'pluginname' in e.image_names
    assert 'labeled' in e.image_names

