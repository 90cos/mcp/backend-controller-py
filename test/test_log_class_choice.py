from controller.logs import Logger
from controller.common import REGISTRY_PREFIX, PLUGIN_PREFIX

class MockDockerService(object):
    def __init__(self, image):
        self.attrs = {"Spec": {"TaskTemplate": {"ContainerSpec": {"Image": image}}}}


def test_default_logger():
    service = MockDockerService("some-random-image")
    logger = Logger(service)
    assert "DefaultLogger" in str(type(logger))


def test_frontend_logger():
    service = MockDockerService(f"{REGISTRY_PREFIX}frontend-ui:deploy@335353535")
    logger = Logger(service)
    assert "Frontend" in str(type(logger))


def test_plugin_logger():
    service = MockDockerService(f"{PLUGIN_PREFIX}interpreter-plugin:deploy")
    logger = Logger(service)
    assert "Plugin" in str(type(logger))
    assert "Windows" not in str(type(logger))


def test_plugin_extra_logger():
    service = MockDockerService(f"{PLUGIN_PREFIX}interpreter-plugin-extra:deploy")
    logger = Logger(service)
    assert "Plugin" in str(type(logger))
    assert "Windows" not in str(type(logger))


def test_plugin_windows_logger():
    service = MockDockerService(f"{PLUGIN_PREFIX}interpreter-plugin-windows:deploy")
    logger = Logger(service)
    assert "Windows" in str(type(logger))
    assert "Plugin" not in str(type(logger))


def test_ramrod_core():
    service = MockDockerService(f"{REGISTRY_PREFIX}database-brain:deploy")
    logger = Logger(service)
    assert "Brain" in str(type(logger))

