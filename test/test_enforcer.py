from controller.dse import enforcer
from controller.common import PLUGIN_PREFIX
from pytest import fixture, raises
import docker
from docker.errors import NotFound, NullResource
from controller.common.controller_common import add_ports, remove_ports
from brain import connect
from brain.static import RPC, RPP
import rethinkdb as r
from os import environ
from time import time, sleep
from copy import deepcopy
from uuid import uuid4

CLIENT = docker.from_env()
rethink_srv = None


TEST_PLUGIN_DATA = {
    "Name": "Harness",
    "ServiceName": "Harness-5000tcp",
    "State": "",
    "DesiredState": "Activate",
    "Interface": "123.33.32.1",
    "OS": "posix",
    "Extra": False,
    "Environment": ["STAGE=DEV", "NORMAL=1"],
    "ExternalPorts": ["5000/tcp"],
    "InternalPorts": ["5000/tcp"]
}

TEST_PLUGIN_UBUNTU16_DATA = {
    "Name": "Harness",
    "ServiceName": "Harness-5044tcp",
    "State": "",
    "DesiredState": "Activate",
    "Interface": "123.33.32.1",
    "OS": "posix",
    "Extra": True,
    "ImageName": "ubuntu1604",
    "Environment": ["STAGE=DEV", "NORMAL=1"],
    "ExternalPorts": ["5044/tcp"],
    "InternalPorts": ["5044/tcp"]
}

TEST_PLUGIN_ALPINE311_DATA = {
    "Name": "Harness",
    "ServiceName": "Harness-5044tcp",
    "State": "",
    "DesiredState": "Activate",
    "Interface": "123.33.32.1",
    "OS": "posix",
    "Extra": True,
    "ImageName": "alpine311",
    "Environment": ["STAGE=DEV", "NORMAL=1"],
    "ExternalPorts": ["5044/tcp"],
    "InternalPorts": ["5044/tcp"]
}

TEST_MULTI_PLUGIN_DATA = {
    "Name": "Harness",
    "ServiceName": "Harness-5000tcp",
    "State": "",
    "DesiredState": "Activate",
    "Interface": "123.33.32.1",
    "OS": "posix",
    "Extra": False,
    "Environment": ["STAGE=DEV", "NORMAL=1"],
    "ExternalPorts": ["5000/tcp", "5000/udp"],
    "InternalPorts": ["5000/tcp", "5000/udp"]
}

TEST_GIGA_MULTI_PLUGIN_DATA = {
    "Name": "Harness",
    "ServiceName": "Harness-5000tcp",
    "State": "",
    "DesiredState": "Activate",
    "Interface": "123.33.32.1",
    "OS": "posix",
    "Extra": False,
    "Environment": ["STAGE=DEV", "NORMAL=1"],
    "ExternalPorts": ["5000/tcp", "5000/udp", "6000/tcp", "6000/udp"],
    "InternalPorts": ["5000/tcp", "5000/udp", "6000/tcp", "6000/udp"]
}

INITIAL_PORT_DATA = {
    "Interface": "123.33.32.1",
    "TCPPorts": [],
    "UDPPorts": [],
    "NodeHostName": "TestNode",
    "OS": "posix"
}

INITIAL_PORT_POP_DATA = {
    "Interface": "123.33.32.1",
    "TCPPorts": ["5000"],
    "UDPPorts": ["5000"],
    "NodeHostName": "TestNode",
    "OS": "posix"
}

TEST_CREATE_DATA = {
    "Name": "Harness",
    "ServiceName": "TestSrv-9876tcp",
    "State": "",
    "DesiredState": "Activate",
    "Interface": "1.1.1.1",
    "OS": "posix",
    "Extra": False,
    "Environment": [
        "STAGE=DEV",
        "NORMAL=1",
        f"RETHINK_HOST={environ.get('RETHINK_HOST', 'localhost')}"
    ],
    "ExternalPorts": ["9876/tcp"],
    "InternalPorts": ["9876/tcp"]
}


def test_changefeed():
    enf = enforcer.Enforcer()
    enf.establish_changefeed()
    RPC.insert(TEST_PLUGIN_DATA).run(connect())
    for change in enf.plugin_changes:
        del(change["new_val"]["id"])
        assert change["new_val"] == TEST_PLUGIN_DATA
        break


def test_api_creates_service():
    new_service = deepcopy(TEST_PLUGIN_DATA)
    new_service["ServiceName"] = "Harness-4444tcp"
    new_service["ExternalPorts"] = ["4444/tcp"]
    new_service["InternalPorts"] = ["4444/tcp"]
    new_service["id"] = str(uuid4())
    service_list = CLIENT.services.list()
    for service in service_list:
        assert new_service["ServiceName"] != service.name

    assert True


def test_port_mapping_1():
    test_data = deepcopy(TEST_PLUGIN_DATA)
    expected_ports = {5000}

    # return  # TODO: don't forward the port from the ingress network because we use host networking

    i = 0
    for p in enforcer.Enforcer.get_endpoint_spec_from_plugin(test_data).get("Ports"):
        i += 1
        assert p.get("PublishedPort") in expected_ports
    assert i > 0


def test_port_mapping_2():
    test_data = deepcopy(TEST_PLUGIN_DATA)
    test_data["ExternalPorts"].append("4000/tcp")
    test_data["InternalPorts"].append("4000/tcp")
    expected_ports = {5000, 4000}

    # return  # TODO: don't forward the port from the ingress network because we use host networking

    i = 0
    for p in enforcer.Enforcer.get_endpoint_spec_from_plugin(test_data).get("Ports"):
        i += 1
        assert p.get("PublishedPort") in expected_ports
    assert i > 0


def test_get_image_from_ubuntu1604_plugin():
    old_tag = environ.get("TAG", "")
    environ['TAG'] = "TEST"
    test_data = deepcopy(TEST_PLUGIN_UBUNTU16_DATA)
    image = enforcer.Enforcer().get_image_from_plugin(test_data)
    assert image == "{plugin_prefix}interpreter-plugin-ubuntu1604:{tag}".format(
        plugin_prefix=PLUGIN_PREFIX,
        tag=environ.get("TAG")
    )
    environ['TAG'] = old_tag


def test_get_image_from_alpine311_plugin():
    old_tag = environ.get("TAG", "")
    environ['TAG'] = "TEST"
    test_data = deepcopy(TEST_PLUGIN_ALPINE311_DATA)
    image = enforcer.Enforcer().get_image_from_plugin(test_data)
    assert image == "{plugin_prefix}interpreter-plugin-alpine311:{tag}".format(
        plugin_prefix=PLUGIN_PREFIX,
        tag=environ.get("TAG")
    )
    environ['TAG'] = old_tag


def test_get_image_from_plugin():
    old_tag = environ.get("TAG", "")
    environ['TAG'] = "TEST"
    test_data = deepcopy(TEST_PLUGIN_DATA)
    image = enforcer.Enforcer().get_image_from_plugin(test_data)
    assert image == "{plugin_prefix}interpreter-plugin:{tag}".format(
        plugin_prefix=PLUGIN_PREFIX,
        tag=environ.get("TAG")
    )
    environ['TAG'] = old_tag


def test_get_image_from_plugin_extra():
    old_tag = environ.get("TAG", "")
    environ['TAG'] = "TEST"
    test_data = deepcopy(TEST_PLUGIN_DATA)
    test_data["Extra"] = True
    image = enforcer.Enforcer().get_image_from_plugin(test_data)
    assert image == "{plugin_prefix}interpreter-plugin-extra:{tag}".format(
        plugin_prefix=PLUGIN_PREFIX,
        tag=environ.get("TAG")
    )
    environ['TAG'] = old_tag


def test_get_image_from_plugin_1():
    old_tag = environ.get("TAG", "")
    environ['TAG'] = "TEST"
    test_data = deepcopy(TEST_PLUGIN_DATA)
    test_data["OS"] = "nt"
    image = enforcer.Enforcer().get_image_from_plugin(test_data)
    assert image == "{plugin_prefix}interpreter-plugin-windows:{tag}".format(
        plugin_prefix=PLUGIN_PREFIX,
        tag=environ.get("TAG")
    )
    environ['TAG'] = old_tag


def test_get_environment_from_plugin_1():
    test_data = deepcopy(TEST_PLUGIN_DATA)
    out_env = enforcer.Enforcer().ensure_populated_environment_variables(test_data)
    assert "STAGE=DEV" in out_env


def test_get_environment_from_plugin_2():
    test_data = deepcopy(TEST_PLUGIN_DATA)
    environ["TEST_ENV_SHOULD_COPY"] = "YES"
    out_env = enforcer.Enforcer().ensure_populated_environment_variables(test_data)
    assert "TEST_ENV_SHOULD_COPY=YES" in out_env


# add_port moved to common
def test_add_port():
    RPP.delete().run(connect())
    enf = enforcer.Enforcer()
    RPP.insert(INITIAL_PORT_DATA).run(connect())
    res = add_ports(TEST_PLUGIN_DATA, connect())
    assert res["errors"] == 0
    port = RPP.run(connect()).next()
    assert port["TCPPorts"][0] == "5000"


def test_add_port_multi():
    RPP.delete().run(connect())
    enf = enforcer.Enforcer()
    RPP.insert(INITIAL_PORT_DATA).run(connect())
    res = add_ports(TEST_MULTI_PLUGIN_DATA, connect())
    assert res["errors"] == 0
    port = RPP.run(connect()).next()
    assert port["TCPPorts"][0] == "5000"
    assert port["UDPPorts"][0] == "5000"


def test_add_port_giga():
    RPP.delete().run(connect())
    enf = enforcer.Enforcer()
    RPP.insert(INITIAL_PORT_DATA).run(connect())
    res = add_ports(TEST_GIGA_MULTI_PLUGIN_DATA, connect())
    assert res["errors"] == 0
    port = RPP.run(connect()).next()
    assert "5000" in port["TCPPorts"]
    assert "6000" in port["TCPPorts"]
    assert "5000" in port["UDPPorts"]
    assert "6000" in port["UDPPorts"]


def test_add_duplicate_port():
    enf = enforcer.Enforcer()
    RPP.insert(INITIAL_PORT_POP_DATA).run(connect())
    assert add_ports(TEST_PLUGIN_DATA, connect())["errors"] != 0


def test_remove_port():
    RPP.delete().run(connect())
    key = RPP.insert(INITIAL_PORT_POP_DATA).run(connect())["generated_keys"][0]
    enf = enforcer.Enforcer()
    remove_ports(TEST_PLUGIN_DATA, connect())
    port = RPP.get(key).run(connect())
    assert "5000" not in port["TCPPorts"]
    assert "5000" in port["UDPPorts"]


def test_get_constraint_from_plugin():
    test_data = deepcopy(TEST_PLUGIN_DATA)
    test_data["Interface"] = "1.1.1.1"
    test_data["OS"] = "posix"
    out_constraints = enforcer.Enforcer.generate_constraints_list(test_data)
    expected = [
            "node.labels.ip==1.1.1.1",
            #"node.labels.os==posix"
        ]
    assert expected == out_constraints


def test_get_constraint_from_plugin_2():
    test_data = deepcopy(TEST_PLUGIN_DATA)
    test_data["Interface"] = "abc-not-an-ip_at_all!"
    test_data["OS"] = "posix"
    out_constraints = enforcer.Enforcer.generate_constraints_list(test_data)
    expected = [
        "node.labels.ip==abc-not-an-ip_at_all!",
        #"node.labels.os==posix"
    ]
    assert expected == out_constraints


def test_get_constraint_from_plugin_3():
    test_data = deepcopy(TEST_PLUGIN_DATA)
    test_data["Interface"] = "abc-not-an-ip_at_all!"
    test_data["OS"] = "nt"
    out_constraints = enforcer.Enforcer.generate_constraints_list(test_data)
    expected = [
        "node.labels.ip==abc-not-an-ip_at_all!",
        #"node.labels.os==nt"
    ]
    assert expected == out_constraints


def test_get_constraint_from_plugin_4():
    test_data = deepcopy(TEST_PLUGIN_DATA)
    test_data["Interface"] = "abc-not-an-ip_at_all!"
    test_data["OS"] = "this is an invalid OSNAME! but there is no error checking here"
    out_constraints = enforcer.Enforcer.generate_constraints_list(test_data)
    expected = [
        "node.labels.ip==abc-not-an-ip_at_all!",
        #"node.labels.os==this is an invalid OSNAME! but there is no error checking here"
    ]
    assert expected == out_constraints


def test_get_healthcheck():
    out_healthcheck = enforcer.Enforcer.generate_healthcheck()
    expected = {
        "timeout": 10**9 * 3,
        "start_period": 10 ** 9 * 5,
        "retries": 3
    }
    assert out_healthcheck.timeout == expected['timeout']
    assert out_healthcheck.start_period == expected['start_period']
    assert out_healthcheck.retries == expected['retries']


def test_get_bad_healthcheck_1():
    with raises(AssertionError):
        out_healthcheck = enforcer.Enforcer.generate_healthcheck(timeout=10**6 - 2)


def test_get_bad_healthcheck_2():
    with raises(AssertionError):
        out_healthcheck = enforcer.Enforcer.generate_healthcheck(start_period= 10**6 - 2)


def test_get_bad_healthcheck_3():
    with raises(AssertionError):
        out_healthcheck = enforcer.Enforcer.generate_healthcheck(retries=0)


def test_get_bad_healthcheck_4():
    with raises(TypeError):
        out_healthcheck = enforcer.Enforcer.generate_healthcheck(retries="ok")


def test_get_bad_healthcheck_5():
    with raises(AssertionError):
        out_healthcheck = enforcer.Enforcer.generate_healthcheck(retries=10**6)


def test_get_networks():
    networks = enforcer.Enforcer.generate_networks()
    assert networks == ["mcp"]


def test_get_networks_posix():
    plugin=deepcopy(TEST_PLUGIN_DATA)
    networks = enforcer.Enforcer.generate_networks(plugin=plugin)
    assert networks == ["mcp"]


def test_get_networks_all():
    plugin = deepcopy(TEST_PLUGIN_DATA)
    plugin["OS"] = "all"
    networks = enforcer.Enforcer.generate_networks(plugin=plugin)
    assert networks == []


def test_get_networks_windows():
    plugin = deepcopy(TEST_PLUGIN_DATA)
    plugin["OS"] = "nt"
    networks = enforcer.Enforcer.generate_networks(plugin=plugin)
    assert networks == []


def test_get_networks_malformed():
    plugin = deepcopy(TEST_PLUGIN_DATA)
    del(plugin["OS"])
    networks = enforcer.Enforcer.generate_networks(plugin=plugin)
    assert networks == ["mcp"]


def test_get_unsupported_networks_1():
    future_capability = ["network-we-do-not-support-today", "and-another"]
    networks = enforcer.Enforcer.generate_networks(desired_networks=future_capability)
    assert networks == future_capability


def test_get_bad_networks_1():
    with raises(AssertionError):
        out_healthcheck = enforcer.Enforcer.generate_networks(desired_networks="not a list")


def test_environment_not_in_env():
    # RETHINK_HOST should always be in environment now
    plugin = deepcopy(TEST_PLUGIN_DATA)
    out_env = enforcer.Enforcer.ensure_populated_environment_variables(plugin)
    found_rethink = False
    for env in out_env:
        if env.startswith("RETHINK_HOST"):
            found_rethink = True
            break
    assert found_rethink


def test_environment_yes_in_in_env():
    plugin = deepcopy(TEST_PLUGIN_DATA)
    rethink_already_exists = "RETHINK_HOST" in environ
    if rethink_already_exists:
        tmp_rethink = environ["RETHINK_HOST"]
    environ['RETHINK_HOST'] = "rethinkdb"
    out_env = enforcer.Enforcer.ensure_populated_environment_variables(plugin)
    found_rethink = False
    for env in out_env:
        if env.startswith("RETHINK_HOST"):
            found_rethink = True
            break
    if not rethink_already_exists:
        del(environ['RETHINK_HOST'])
    else:
        environ["RETHINK_HOST"] = tmp_rethink
    assert found_rethink


def test_environment_windows_needs_public_ip():
    plugin = deepcopy(TEST_PLUGIN_DATA)
    plugin['OS'] = 'nt'
    out_env = enforcer.Enforcer.ensure_populated_environment_variables(plugin)
    found_rethink = False
    for env in out_env:
        if env.startswith("RETHINK_HOST"):
            found_rethink = True
            assert "rethinkdb" not in env
    assert found_rethink


def test_environment_all_includes_windows_needs_public_ip():
    plugin = deepcopy(TEST_PLUGIN_DATA)
    plugin['OS'] = 'all'
    out_env = enforcer.Enforcer.ensure_populated_environment_variables(plugin)
    found_rethink = False
    for env in out_env:
        if env.startswith("RETHINK_HOST"):
            found_rethink = True
            assert "rethinkdb" not in env
    assert found_rethink


def test_environment_unsupported_pretends_to_be_windows_needs_public_ip():
    plugin = deepcopy(TEST_PLUGIN_DATA)
    plugin['OS'] = 'some-other-unsupported-os'
    out_env = enforcer.Enforcer.ensure_populated_environment_variables(plugin)
    found_rethink = False
    for env in out_env:
        if env.startswith("RETHINK_HOST"):
            found_rethink = True
            assert "rethinkdb" not in env
    assert found_rethink


def test_restart_policy_1():
    out_rp = enforcer.Enforcer.generate_restart_policy()
    defaults = {
            "condition": "on-failure",
            "max_attempts": 3
        }
    assert out_rp.get("Condition") == defaults['condition']
    assert out_rp.get("MaxAttempts") == defaults['max_attempts']


def test_restart_policy_2():
    out_rp = enforcer.Enforcer.generate_restart_policy(condition="any")
    user_override = {
        "condition": "any",
        "max_attempts": 3
    }
    assert out_rp.get("Condition") == user_override['condition']
    assert out_rp.get("MaxAttempts") == user_override['max_attempts']


def test_restart_policy_3():
    out_rp = enforcer.Enforcer.generate_restart_policy(condition="none")
    user_override = {
        "condition": "none",
        "max_attempts": 3
    }
    assert out_rp.get("Condition") == user_override['condition']
    assert out_rp.get("MaxAttempts") == user_override['max_attempts']


def test_restart_policy_4():
    out_rp = enforcer.Enforcer.generate_restart_policy(max_attempts=200)
    user_override = {
        "condition": "on-failure",
        "max_attempts": 200
    }
    assert out_rp.get("Condition") == user_override['condition']
    assert out_rp.get("MaxAttempts") == user_override['max_attempts']


def test_bad_restart_policy_1():
    with raises(AssertionError):
        out_rp = enforcer.Enforcer.generate_restart_policy(condition="something else unchecked")


def test_bad_restart_policy_2():
    with raises(TypeError):
        out_rp = enforcer.Enforcer.generate_restart_policy(max_attempts="something else unchecked")


def test_bad_restart_policy_3():
    with raises(AssertionError):
        out_rp = enforcer.Enforcer.generate_restart_policy(max_attempts=-2)


def test_generate_update_config_1():
    out_uc = enforcer.Enforcer.generate_update_config()
    assert out_uc.get("Parallelism") == 0
    assert out_uc.get("Delay") == 0


def test_generate_update_config_2():
    out_uc = enforcer.Enforcer.generate_update_config(parallelism=3)
    assert out_uc.get("Parallelism") == 3
    assert out_uc.get("Delay") == 0


def test_generate_update_config_3():
    with raises(AssertionError):
        out_uc = enforcer.Enforcer.generate_update_config(parallelism=3, delay=77)


def test_generate_update_config_4():
    with raises(TypeError):
        out_uc = enforcer.Enforcer.generate_update_config(parallelism=3, delay="not a real thing")


def test_generate_update_config_5():
    with raises(AssertionError):
        out_uc = enforcer.Enforcer.generate_update_config(parallelism=3, delay=-5)


def test_generate_update_config_6():
    with raises(AssertionError):
        out_uc = enforcer.Enforcer.generate_update_config(parallelism=3, delay=10**4)


def test_generate_update_config_7():
    out_uc = enforcer.Enforcer.generate_update_config(parallelism=0, delay=10**7)
    assert out_uc.get("Parallelism") == 0
    assert out_uc.get("Delay") == 10**7


def test_generate_update_config_8():
    with raises(AssertionError):
        out_uc = enforcer.Enforcer.generate_update_config(parallelism=-1, delay=0)


def test_mounts_1():
    out_m = enforcer.Enforcer.generate_mounts()
    assert out_m == []


def test_mounts_2():
    out_m = enforcer.Enforcer.generate_mounts(mounts=["a:a:ro", "b:b:rw"])
    assert out_m == ["a:a:ro", "b:b:rw"]


def test_dns_config_1():
    from docker.types import DNSConfig
    out_d = enforcer.Enforcer.generate_dns_config()
    assert isinstance(out_d, DNSConfig)


def test_service_create_spec_1():
    environ["TAG"] = "dev"
    test_data = deepcopy(TEST_PLUGIN_DATA)
    test_data["Interface"] = "1.1.1.1"
    out_svc = enforcer.Enforcer().generate_service_create(test_data)
    image_name = enforcer.Enforcer().get_image_from_plugin(test_data)
    CLIENT.services.create(image_name, **out_svc)
    sleep(16)  # slow ci
    assert test_data["ServiceName"] in [x.name for x in CLIENT.services.list()]


def test_enforcer_create_service():
    environ["TAG"] = "dev"
    test_data = TEST_CREATE_DATA
    out_svc = enforcer.Enforcer().generate_service_create(test_data)
    image_name = enforcer.Enforcer().get_image_from_plugin(test_data)
    enforcer.Enforcer().create_service(test_data)
    sleep(16)  # slow ci
    assert test_data["ServiceName"] in [x.name for x in CLIENT.services.list()]


def test_enforcer_remove_service():
    found_srv = None
    test_data = deepcopy(TEST_CREATE_DATA)
    srv_list = CLIENT.services.list()
    for srv in srv_list:
        if srv.name == test_data["ServiceName"]:
            found_srv = srv
    assert found_srv.id
    test_data["ServiceID"] = found_srv.id
    enforcer.Enforcer().remove_service(test_data)
    sleep(16)  # slow ci
    assert test_data["ServiceName"] not in [x.name for x in CLIENT.services.list()]


def test_enforcer_restart_service():
    environ["TAG"] = "dev"
    test_data = TEST_CREATE_DATA
    out_svc = enforcer.Enforcer().generate_service_create(test_data)
    image_name = enforcer.Enforcer().get_image_from_plugin(test_data)
    enf = enforcer.Enforcer()
    enf.create_service(test_data)
    sleep(16)  # slow ci
    assert test_data["ServiceName"] in [x.name for x in CLIENT.services.list()]

    found_srv = None
    srv_list = CLIENT.services.list()
    for srv in srv_list:
        if srv.name == test_data["ServiceName"]:
            found_srv = srv
    assert found_srv.id
    test_data["ServiceID"] = found_srv.id

    enf.restart_service(test_data)
    assert found_srv.id in enf.restarting_svc
    sleep(16)  # slow ci
    assert test_data["ServiceName"] not in [x.name for x in CLIENT.services.list()]


def test_enforcer_handle_change_activate():
    change = {}
    change["new_val"] = deepcopy(TEST_CREATE_DATA)
    cnv = change["new_val"]
    enf = enforcer.Enforcer()
    enf._handle_change(change)
    sleep(16)  # slow ci
    assert cnv["ServiceName"] in [x.name for x in CLIENT.services.list()]


def test_enforcer_handle_change_stop():
    change = {}
    change["new_val"] = deepcopy(TEST_CREATE_DATA)
    cnv = change["new_val"]
    cnv["DesiredState"] = "Stop"

    found_srv = None
    srv_list = CLIENT.services.list()
    for srv in srv_list:
        if srv.name == cnv["ServiceName"]:
            found_srv = srv
    assert found_srv.id
    cnv["ServiceID"] = found_srv.id

    enf = enforcer.Enforcer()
    enf._handle_change(change)
    sleep(16)  # slow ci
    assert cnv["ServiceName"] not in [x.name for x in CLIENT.services.list()]


def test_enforcer_handle_change_restart():
    change = {}
    change["new_val"] = deepcopy(TEST_CREATE_DATA)
    cnv = change["new_val"]
    enf = enforcer.Enforcer()
    enf._handle_change(change)  # activate
    sleep(16)  # slow ci
    assert cnv["ServiceName"] in [x.name for x in CLIENT.services.list()]

    change["new_val"] = deepcopy(TEST_CREATE_DATA)
    cnv = change["new_val"]
    cnv["DesiredState"] = "Restart"

    found_srv = None
    srv_list = CLIENT.services.list()
    for srv in srv_list:
        if srv.name == cnv["ServiceName"]:
            found_srv = srv
    assert found_srv.id
    cnv["ServiceID"] = found_srv.id

    enf._handle_change(change)  # restart
    sleep(6)
    assert cnv["ServiceName"] not in [x.name for x in CLIENT.services.list()]


def test_enforcer_handle_change_blank_stopped():
    change = {}
    change["new_val"] = deepcopy(TEST_CREATE_DATA)
    cnv = change["new_val"]
    enf = enforcer.Enforcer()
    enf._handle_change(change)  # activate
    sleep(16)  # slow ci
    assert cnv["ServiceName"] in [x.name for x in CLIENT.services.list()]

    change["new_val"] = deepcopy(TEST_CREATE_DATA)
    cnv = change["new_val"]
    cnv["DesiredState"] = "Restart"

    found_srv = None
    srv_list = CLIENT.services.list()
    for srv in srv_list:
        if srv.name == cnv["ServiceName"]:
            found_srv = srv
    assert found_srv.id
    cnv["ServiceID"] = found_srv.id

    enf._handle_change(change)  # restart
    sleep(16)  # slow ci
    assert cnv["ServiceName"] not in [x.name for x in CLIENT.services.list()]

    cnv["DesiredState"] = ""
    cnv["State"] = "Stopped"
    enf._handle_change(change)  # reactivate
    sleep(16)  # slow ci
    assert cnv["ServiceName"] in [x.name for x in CLIENT.services.list()]

    srv_list = CLIENT.services.list()
    for srv in srv_list:
        if srv.name == cnv["ServiceName"]:
            found_srv = srv
    assert found_srv.id
    cnv["ServiceID"] = found_srv.id
    enf.remove_service(cnv)  # cleanup
    sleep(16)  # slow ci
    assert cnv["ServiceName"] not in [x.name for x in CLIENT.services.list()]


def test_enforcer_remove_terrible_id():
    environ["TAG"] = "dev"
    test_data = TEST_CREATE_DATA
    enforcer.Enforcer().create_service(test_data)
    sleep(6)
    assert test_data["ServiceName"] in [x.name for x in CLIENT.services.list()]

    found_srv = None
    test_data = deepcopy(TEST_CREATE_DATA)
    srv_list = CLIENT.services.list()
    for srv in srv_list:
        if srv.name == test_data["ServiceName"]:
            found_srv = srv
    assert found_srv.id
    test_data["ServiceID"] = "441561411474153148784351515"
    with raises(NotFound):
        enforcer.Enforcer().remove_service(test_data)
    test_data["ServiceID"] = 4531518
    with raises(ValueError):
        enforcer.Enforcer().remove_service(test_data)
    test_data["ServiceID"] = None
    with raises(NullResource):
        enforcer.Enforcer().remove_service(test_data)
    sleep(16)  # slow ci

    test_data["ServiceID"] = found_srv.id
    enforcer.Enforcer().remove_service(test_data)
    sleep(16)  # slow ci
    assert test_data["ServiceName"] not in [x.name for x in CLIENT.services.list()]

def test_cleanup():
    found_srv = None
    test_data = deepcopy(TEST_PLUGIN_DATA)
    srv_list = CLIENT.services.list()
    for srv in srv_list:
        if srv.name == test_data["ServiceName"]:
            found_srv = srv
    assert found_srv.id
    test_data["ServiceID"] = found_srv.id
    found_srv.remove()
    sleep(16)  # slow ci
    assert test_data["ServiceName"] not in [x.name for x in CLIENT.services.list()]
