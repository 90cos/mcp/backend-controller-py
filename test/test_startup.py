import os
from os import environ
from controller.common.controller_common import get_manager_ip
from controller.dse.enforcer import Enforcer
from time import sleep
from brain.static import RPC, RPP, RBL
from brain import connect
from brain.controller.plugins import create_plugin
from copy import deepcopy
# from uuid import uuid4
import docker
import controller.startup.service_startup as startup


cli = docker.from_env()


TEST_CREATE_DATA = {
    "Name": "Harness",
    "ServiceName": "TestSrv-9876tcp",
    "State": "",
    "DesiredState": "Activate",
    "Interface": get_manager_ip(),
    "OS": "posix",
    "Extra": False,
    "ServiceID": "",
    "Environment": ["PLUGIN=Harness", "STAGE=DEV", "NORMAL=1"],
    "ExternalPorts": ["9876/tcp"],
    "InternalPorts": ["9876/tcp"]
}

AUX_INFO = {
        "Name": "AuxServices",
        "ServiceName": "AuxiliaryServices",
        "State": "",
        "DesiredState": "",
        "Interface": get_manager_ip(),
        "OS": "posix",
        "Extra": False,
        "Environment": ["STAGE={}".format(os.getenv("STAGE", "TESTING").replace("TESTING", "DEV", 1)),
                        "LOGLEVEL={}".format(os.getenv("LOGLEVEL", "DEBUG")),
                        "TAG={}".format(os.getenv("TAG", "dev")),
                        "RETHINK_HOST={}".format(environ.get("RETHINK_HOST", "localhost"))
                        ],
        "ExternalPorts": ["20/tcp","21/tcp","80/tcp","443/tcp","10090/tcp","10091/tcp","10092/tcp","10093/tcp","10094/tcp","10095/tcp","10096/tcp","10097/tcp","10098/tcp","10099/tcp","10100/tcp"],
        "InternalPorts": ["20/tcp","21/tcp","80/tcp","443/tcp","10090/tcp","10091/tcp","10092/tcp","10093/tcp","10094/tcp","10095/tcp","10096/tcp","10097/tcp","10098/tcp","10099/tcp","10100/tcp"]
    }


def test_populate_existing_services():
    enf = Enforcer()
    harn = deepcopy(TEST_CREATE_DATA)
    harn['ServiceName'] = "TestSrv-2222tcp"
    harn['ExternalPorts'] = ["2222/tcp"]
    harn['InternalPorts'] = ["2222/tcp"]
    harn['DesiredState'] = ""
    harn['State'] = "Active"
    enf.create_service(harn)
    assert len(list(RPC.filter({"ServiceName": "TestSrv-2222tcp"}).run(connect()))) == 0
    sleep(10)  # because docker will get around to actually doing it at some point
    startup.verify_service_status()
    assert len(list(RPC.filter({"ServiceName": "TestSrv-2222tcp"}).run(connect()))) == 1


def test_expected_services_are_running():
    (expected, actual) = startup.verify_expected_active_services()
    assert expected.issubset(actual)


def test_expected_services_are_not_running():
    harn = deepcopy(TEST_CREATE_DATA)
    harn['ServiceName'] = "TestSrv-7777tcp"
    harn['ExternalPorts'] = ["7777/tcp"]
    harn['InternalPorts'] = ["7777/tcp"]
    harn['DesiredState'] = ""
    harn['State'] = "Active"
    create_plugin(harn)
    (expected, actual) = startup.verify_expected_active_services()
    assert not expected.issubset(actual)
    found_log = False
    for log in RBL.run(connect()):
        if log['msg'].startswith("expected TestSrv-7777tcp"):
            found_log = True
    assert found_log


def test_fetch_aux():
    fetched_aux = startup.fetch_aux()
    assert fetched_aux == AUX_INFO


def test_create_aux_service():
    RPP.delete().run(connect())
    container = startup.create_aux_service(AUX_INFO)
    sleep(5)
    assert "auxiliary-services" in [x.name for x in cli.containers.list()]
    container.stop()


def test_create_aux_service_duplicate():
    RPP.delete().run(connect())
    container = startup.create_aux_service(AUX_INFO) # create initial
    sleep(5)
    assert "auxiliary-services" in [x.name for x in cli.containers.list()]
    startup.startup_services()
    container.stop()


def test_startup_services():
    found = False
    assert "auxiliary-services" not in [x.name for x in cli.containers.list()]
    RPP.delete().run(connect())
    RPC.delete().run(connect())
    os.environ["START_AUX"] = "YES"
    startup.startup_services()
    sleep(5)
    del(os.environ["START_AUX"])
    containers = cli.containers.list()
    for container in containers:
        if container.name == "auxiliary-services":
            container.stop()
            found = True
    assert found
