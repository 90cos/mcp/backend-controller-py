from os import environ

# environment may or may not have a trailing slash, be sure there is only one trailing slash
# images may be distributed without a named registry, must allow for name:tag as well.
REGISTRY_PREFIX = environ.get("REGISTRY_PREFIX", "")
if REGISTRY_PREFIX and not REGISTRY_PREFIX.endswith("/"):
    REGISTRY_PREFIX += "/"


# plugins may be distributed from a separate repository from the core systems
PLUGIN_PREFIX = environ.get("PLUGIN_PREFIX", "")
if PLUGIN_PREFIX and not PLUGIN_PREFIX.endswith("/"):
    PLUGIN_PREFIX += "/"

PLUGIN_BASE = "interpreter-plugin"
