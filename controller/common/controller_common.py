from brain import connect
from brain.static import RPC, RPP, RBL, ACTIVE, STOP, ERROR
from brain.controller.plugins import find_plugin
from brain.controller.interfaces import create_port
from brain.static import RPC, RPP, RBL
from brain import connect
from brain import connect
from brain.static import RPC, RPP, RBL
from time import time
import docker
from . import PLUGIN_PREFIX


def add_ports(plugin_data, conn):
    """
    Attempt to add ports to the Port table in the brain
    :param plugin_data: dict with structure that matches the Plugin table schema.
    :return: dict containing rethinkdb response.
    """
    port_dict = {
        "Interface": plugin_data["Interface"],
        "TCPPorts": [],
        "UDPPorts": [],
        "OS": plugin_data["OS"],
        "NodeHostName": ""
    }
    for entry in plugin_data["ExternalPorts"]:
        port, proto = entry.split("/")
        if proto == "tcp":
            port_dict["TCPPorts"].append(port)
        elif proto == "udp":
            port_dict["UDPPorts"].append(port)

    return create_port(port_dict, conn)


def remove_ports(plugin_data, conn):
    """
    Remove the ports of a plugin service from the brain ports table
    :param plugin_data: <dict> matching the plugins schema
    :return: None
    """
    # removed_ports = list(brain.controller.interfaces.get_ports_by_ip(plugin_data["Interface"], conn=self.conn))
    port_obj = RPP.filter({"Interface": plugin_data["Interface"]}).run(conn).next()
    for entry in plugin_data["ExternalPorts"]:
        try:
            port, proto = entry.split("/")
            if proto == "tcp":
                port_obj["TCPPorts"].remove(port)
            elif proto == "udp":
                port_obj["UDPPorts"].remove(port)
        except ValueError:
            # Value error from trying to remove a port that is not logged in database
            # removing ports is safe to do if one port is not found. go to next port
            continue

    RPP.filter({"Interface": plugin_data["Interface"]}).update(port_obj).run(conn)


def log(msg, conn,
        severity=5,
        service_name="",
        source_host="",
        details=None):

    log = {
        "rt": int(time() * 1000),
        "msg": msg,
        "Severity": severity,
        "sourceServiceName": service_name,
        "shost": source_host
    }
    if details and isinstance(details, dict):
        log["msgDoc"] = details
    val = RBL.insert(log).run(conn)
    val


def log_error(conn, msg, rethink_response):
    """

    :param conn:
    :param msg:
    :param rethink_response:
    :return:
    """
    if rethink_response["updated"] == 0: #TODO: don't bother looking at rethink response and do what the caller asked to be done
        log(msg, conn)


def update_service_id_rpc(conn, service_name, service_id):
    """
    Updates RPC by filtering ServiceName and update
    ServiceID with plugin's service id.
    :param conn: brain's connection
    :param service_name:
    :param service_id:
    :return:
    """
    update_sid = RPC.filter({"ServiceName": service_name}).update({"ServiceID": service_id,
                                                                   "DesiredState": "",
                                                                   "State": "Active"}).run(conn)

    return update_sid


def update_states_rpc(conn, service_name, plugins_state):
    """
    Updates RPC by filtering ServiceName, DesiredState
    and updates State and DesiredState
    :param conn: brain's connection
    :param service_name:
    :param plugins_state:
    :return:
    """

    update_state = RPC.filter({"ServiceName": service_name}).update({"State": plugins_state,
                                                                     "DesiredState": ""}).run(conn)

    return update_state


def get_manager_ip():
    for node in docker.from_env().nodes.list():
        if node.attrs["Spec"]["Role"] == "manager":
            return node.attrs["Status"]["Addr"]

    return ""


def get_leader_host_name():
    for node in docker.from_env().nodes.list():
        if node.attrs["Spec"]["Role"] == "worker":
            continue
        if node.attrs["ManagerStatus"]["Leader"]:
            return node.attrs["Description"]["Hostname"]


def get_plugin_type_from_image(image_name):
    result = ""
    image_prefix = f"{PLUGIN_PREFIX}interpreter-plugin"
    if image_name.startswith(image_prefix):
        result = remove_prefix(image_prefix, image_name).lstrip("-")
        result = result.split(":")[0]
    return result


def remove_prefix(prefix, data):
    result = ""
    if data.startswith(prefix):
        result = data[len(prefix):]
    return result
