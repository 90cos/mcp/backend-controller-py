import docker
from brain import connect
from brain.static import RPP, RPC, PROD, DEV, STAGE_KEY
from brain.controller.plugins import get_names
import json
from sys import stderr
from os import environ


MANIFEST_FILE = "manifest.json"


def map_os(os):
    if os == "linux" or os == "posix":
        return "posix"
    elif os == "windows" or os == "nt":
        return "nt"
    else:
        return None


# Advertise node
def advertise_nodes():
    return advertise_ip(prepare_nodes())


def build_port_from_node(node):
    ip = node.attrs["Status"]["Addr"]
    host_name = node.attrs["Description"]["Hostname"]
    os_name = str(map_os(node.attrs["Description"]["Platform"]["OS"]))
    new_port = {
        "id": node.id,
        "Interface": ip,
        "NodeHostName": host_name,
        "OS": os_name,
        "TCPPorts": [],
        "UDPPorts": []
    }
    return new_port


def update_node(node):
    node_spec = node.attrs["Spec"]
    node_spec["Labels"] = {
        "ip": node.attrs["Status"]["Addr"],
        "os": str(map_os(node.attrs["Description"]["Platform"]["OS"]))  # convert to posix/nt.
    }
    node.update(node_spec)


def advertise_ip(nodes):
    if len(nodes) < 1:
        return False
    for node in nodes:
        RPP.insert(node, conflict="replace").run(connect())
    return True


def prepare_nodes():
    cli = docker.from_env()
    node_list = cli.nodes.list()
    nodes = []
    for node in node_list:
        # build ports entry
        nodes.append(build_port_from_node(node))

        # update node
        update_node(node)

    return nodes


# Advertise Plugins
def advertise_plugins(filename=None):
    return plugin_advertise(get_plugins(filename))


# get json in the manifest.json file
def get_plugins(filename=None):
    plugin_list = None
    if not filename:
        filename = MANIFEST_FILE
    with open(filename) as manifest:
        plugin_list = json.load(manifest)
    # if manifest looks like the testing manifest, don't import it.
    # legacy packaging should have overwritten this manifest
    # modern packaging will leave it there
    if environ.get(STAGE_KEY, DEV) == PROD and \
            len(plugin_list) == 3 and \
            plugin_list[0]["Name"] == "Harness" and \
            plugin_list[1]["Name"] == "WinHarness" and \
            plugin_list[2]["Name"] == "AllHarness":
        plugin_list = None
    return plugin_list


# insert plugins into RPC
def plugin_advertise(plugin_list):
    if plugin_list is None:
        return False
    else:
        plugins = prepare_plugins(plugin_list)
        known_plugins = get_names()
        for plugin in plugins:
            if plugin["Name"] and plugin["Name"] not in known_plugins:
                RPC.insert(plugin).run(connect())
        return True


# turn manifest JSON into a list of plugin entries
def prepare_plugins(plugin_list):
    plugins = []
    for plugin in plugin_list:
        try:
            plugins.append(make_plugin(plugin))
        except KeyError as kerr:
            stderr.write("Manifest Error, not importing \n\t")
            stderr.write(str(kerr))
            stderr.write("\n")
            continue
    return plugins


# create a single plugin entry from a JSON manifest dictionary
def make_plugin(plugin):
    return {
        "Name": plugin["Name"],
        "ServiceID": "",
        "ServiceName": "",
        "DesiredState": "",
        "State": "Available",
        "Interface": "",
        "ExternalPorts": [],
        "InternalPorts": [],
        "OS": plugin["OS"],
        "Environment": [],
        "Extra": plugin["Extra"],
        "ImageName": plugin.get("ImageName", "")
    }
