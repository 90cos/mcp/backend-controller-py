"""
    This pattern of location is the *new* method for identifying plugin images.

    The plugin image should be in the form of <registry>/interpreter-plugin-<Name>:<tag>
"""

import docker
from ..common.controller_common import get_plugin_type_from_image, remove_prefix
from ..common import PLUGIN_BASE, PLUGIN_PREFIX
from .advertise import plugin_advertise


# legacy image names originate from the legacy plugin builder
# https://gitlab.com/90cos/mcp/backend-interpreter/-/blob/master/.gitlab-ci.yml
# current intent is intended acceptable simultaneous operation with legacy containers with modern containers
#                   but to convert legacy images to modern over time as they are touched.

LEGACY_IMAGE_NAMES = {
    f"{PLUGIN_BASE}-alpine311",
    f"{PLUGIN_BASE}-extra",
    f"{PLUGIN_BASE}-ubuntu1604",
    f"{PLUGIN_BASE}"
}


def modern_plugins() -> list:
    """
    This should query docker and create the "manifest" entry for "discovered" plugin images.

    Returns: list<dict>

    """
    plugins = []
    for image in docker.from_env().images.list():
        for tag in image.tags:
            tag = remove_prefix(PLUGIN_PREFIX, tag).split(":")[0]
            if PLUGIN_BASE in tag and tag not in LEGACY_IMAGE_NAMES:
                if image.labels and "PLUGIN_NAME" in image.labels:
                    name = image.labels["PLUGIN_NAME"]
                else:
                    name = get_plugin_type_from_image(
                        f"{PLUGIN_PREFIX}{tag}"  # reassemble "correct" plugin name
                    ).capitalize()  # make the best of it
                # derived image name is as follows
                # <registry>/interpreter-plugin-imagename:tag
                #                               ^^^^^^^^^
                derived_image_name = remove_prefix(
                    f"{PLUGIN_BASE}-",
                    tag
                )
                plugins.append({
                    "Name": name,
                    "OS": "posix",  # manager must run on POSIX, need to figure out how to collect from nt nodes
                    "Extra": True,  # due to Legacy container rules, Extra must be set to true for modern images
                    "ImageName": derived_image_name
                })
    return plugins


def image_names() -> set:
    """
    ideally only called once at startup (probably by DSE)

    Returns: set of image names located by this module.

    """
    return set([x["ImageName"] for x in modern_plugins()])


def import_modern_plugins():
    """
    loads the plugins into RPC

    Returns: <None>
    """
    plugins = modern_plugins()
    plugin_advertise(plugins)
