import docker
import docker.errors
import os
from os import environ
from sys import stderr
from controller.common.controller_common import get_manager_ip, get_leader_host_name, add_ports, log
from controller.common.controller_common import get_plugin_type_from_image, remove_prefix
from controller.dse.enforcer import Enforcer
from brain.static import RPC, RPP, ACTIVE
from brain import connect
from brain.controller.plugins import find_plugin, create_plugin, update_plugin, get_plugins
from ..common import REGISTRY_PREFIX, PLUGIN_PREFIX

AUX_PORTS = [
    ["20", "tcp"],
    ["21", "tcp"],
    ["80", "tcp"],
    # ["53", "tcp"], Removed- may conflict with host resolver avahi (aux does not do dns anyway)
    ["443", "tcp"],
    ["10090", "tcp"],
    ["10091", "tcp"],
    ["10092", "tcp"],
    ["10093", "tcp"],
    ["10094", "tcp"],
    ["10095", "tcp"],
    ["10096", "tcp"],
    ["10097", "tcp"],
    ["10098", "tcp"],
    ["10099", "tcp"],
    ["10100", "tcp"]
]

AUX_IMG_NAME = "{registry_prefix}auxiliary-services:{tag}".format(
    registry_prefix=REGISTRY_PREFIX,
    tag=os.getenv("TAG", "latest")
)


def verify_service_status():
    """
    magic calls here

    RPC must be updated with existing services
    RPP must be updated with the ports those services are using.

    :return: None
    """
    service_list = docker.from_env().services.list()
    brain_conn = connect(host=os.getenv("RETHINK_HOST"))
    for service in service_list:
        found = find_plugin(service.id, "ServiceID", conn=brain_conn)
        plugin_entry = service_to_plugin(service)
        if plugin_entry and found:
            update_plugin(plugin_entry, conn=brain_conn)
            ap = add_ports(plugin_entry, conn=brain_conn)
            if ap.get("errors", 0) > 0:
                msg = ap.get("first_error", "duplicate port on recovery - create")
                log(msg, brain_conn)
        elif plugin_entry and not found:
            create_plugin(plugin_entry, conn=brain_conn)
            ap = add_ports(plugin_entry, conn=brain_conn)
            if ap.get("errors", 0) > 0:
                msg = ap.get("first_error", "duplicate port on recovery - update")
                log(msg, brain_conn)
        else:
            print("discovered service {}, {} not logging to RPC".format(service.name, service.id))


def verify_expected_active_services():
    brain_conn = connect(host=os.getenv("RETHINK_HOST"))
    plugins = get_plugins(conn=brain_conn)
    service_names = set([service.name for service in docker.from_env().services.list()])
    expected_services = set()
    for plugin in plugins:
        if plugin["ServiceName"] and plugin["State"] == ACTIVE:
            expected_services.add(plugin['ServiceName'])
            if plugin['ServiceName'] not in service_names:
                msg = "expected {}, not in {}".format(plugin['ServiceName'], service_names)
                log(msg, brain_conn)
    return expected_services, service_names


def service_to_plugin(service):
    """
    looking for plugin services

    "<registry>/interpreter-plugin"
    "<registry>/interpreter-plugin-extra"
    "<registry>/interpreter-plugin-windows"



    :param service:
    :return:
    """
    plugin = {}
    # get service name
    image = service.attrs["Spec"]['TaskTemplate']['ContainerSpec']['Image']
    #
    if image.startswith(f"{PLUGIN_PREFIX}interpreter-plugin"):
        plugin['Name'] = ""
        plugin["ServiceName"] = service.name
        # get service id
        plugin["ServiceID"] = service.id
        plugin["DesiredState"] = ""
        plugin["State"] = ACTIVE
        plugin["Interface"] = ""
        plugin["OS"] = "posix"  # default to posix
        plugin["ImageName"] = get_plugin_type_from_image(image)
        # confirm os
        for placement in service.attrs["Spec"]["TaskTemplate"]['Placement']['Constraints']:
            if placement.startswith("node.labels.os="):
                plugin["OS"] = remove_prefix("node.labels.os=", placement)
            elif placement.startswith("node.labels.ip="):
                plugin["Interface"] = remove_prefix("node.labels.ip=", placement)
        plugin["ExternalPorts"] = []
        plugin["InternalPorts"] = []
        for port in service.attrs["Spec"]["EndpointSpec"]['Ports']:
            plugin["ExternalPorts"].append("{port}/{proto}".format(port=port["PublishedPort"], proto=port['Protocol']))
            plugin["InternalPorts"].append("{port}/{proto}".format(port=port["TargetPort"], proto=port['Protocol']))
        # get environment from service
        plugin["Environment"] = []
        for env in service.attrs["Spec"]["TaskTemplate"]["ContainerSpec"]["Env"]:  # does this appear?
            plugin["Environment"].append(env)
            if env.startswith("PLUGIN="):
                plugin["Name"] = remove_prefix("PLUGIN=", env)
        return plugin


def fetch_aux():
    aux_info = {
        "Name": "AuxServices",
        "ServiceName": "AuxiliaryServices",
        "State": "",
        "DesiredState": "",
        "Interface": get_manager_ip(),
        "OS": "posix",
        "Extra": False,
        "Environment": ["STAGE={}".format(os.getenv("STAGE", "TESTING").replace("TESTING", "DEV", 1)),
                        "LOGLEVEL={}".format(os.getenv("LOGLEVEL", "DEBUG")),
                        "TAG={}".format(os.getenv("TAG", "dev")),
                        "RETHINK_HOST={}".format(environ.get("RETHINK_HOST", "localhost"))
                        ],
        "ExternalPorts": [],
        "InternalPorts": []
    }
    for port in AUX_PORTS:
        aux_info["ExternalPorts"].append("/".join(port))
        aux_info["InternalPorts"].append("/".join(port))

    return aux_info


def startup_services():
    if os.getenv("START_AUX") == "YES":
        aux_info = fetch_aux()
        try:
            create_aux_service(aux_info)
            # advertise new service
            # RPC.insert(aux_info).run(connect(host=os.getenv("RETHINK_HOST")))
        except docker.errors.APIError:
            pass


def advertise_startup_service(service):
    new_tcp = []
    new_udp = []
    port = RPP.filter({"NodeHostName": get_leader_host_name()}).run(connect(host=os.getenv("RETHINK_HOST")))
    for tcp in port["TCPPorts"]:
        new_tcp.append(tcp)
    for udp in port["UDPPorts"]:
        new_udp.append(udp)


def create_aux_service(plugin_info):
    client = docker.from_env()
    if "auxiliary-services" in client.containers.list():
        container_data = None
        stderr.write("auxiliary-services already appears to be running, not attempting to start a new one\n")
    else:
        aux_port_dict = {}
        for port, proto in AUX_PORTS:
            aux_port_dict["{port}/{proto}".format(port=port, proto=proto)] = int(port)

        container_info = {
            "name": "auxiliary-services",
            "ports": aux_port_dict,
            "detach": True,
            "auto_remove": True,
            "cap_add": ["SYS_ADMIN"],
            "devices": ["/dev/fuse:/dev/fuse:mrw"],
            "network": "mcp",
            "privileged": True,
            "security_opt": ["apparmor:unconfined"],
        }

        container_data = client.containers.run(AUX_IMG_NAME, **container_info)
    return container_data

