import os
from os import environ
from json import dumps

from docker.errors import NotFound, APIError  # need to import this before docker to avoid a problem
from docker.types import ServiceMode, EndpointSpec, Healthcheck, RestartPolicy, UpdateConfig, DNSConfig
import docker
from ..common.controller_common import log, RPC, add_ports, remove_ports
from ..common import PLUGIN_PREFIX
from brain import connect, r
from time import time
import socket  # for socket.error
"""
This should perform the steps required to

read changes from the brain
call the appropriate docker commands to start a service
"""


class Enforcer(object):
    def __init__(self):
        # intentional deferred import, else this would throw circular import errors
        from ..startup.locate import image_names
        self.tag = environ.get("TAG", "latest")
        self.client = docker.from_env()
        self.image_names = {
            "alpine311",
            "ubuntu1604",
            "extra"
        }.union(image_names())
        self.UBUNTU_NAME = f"{PLUGIN_PREFIX}interpreter-plugin-extra"
        self.ALPINE_NAME = f"{PLUGIN_PREFIX}interpreter-plugin"
        self.plugin_changes = None
        self.restarting_svc = set()

    def start(self):
        self.establish_changefeed()
        # iterate forever
        for change in self.loop_changes():
            self._handle_change(change)
        log("Changefeed ended.", connect())

    def loop_changes(self, wait=3, restart_changefeed=100):
        restart_time = time() + restart_changefeed
        while True:
            try:
                next_item = self.plugin_changes.next(wait=wait)
                yield next_item
            except r.errors.ReqlTimeoutError:
                if time() > restart_time:
                    self.establish_changefeed()
                    restart_time = time() + restart_changefeed
            except socket.error:
                self.establish_changefeed()

    def _handle_change(self, change):
        desired = change["new_val"]["DesiredState"]
        current = change["new_val"]["State"]
        if desired == "Activate" and current != "Active":
            print(change)
            try:
                self.create_service(change["new_val"])
            except APIError as err:
                # notify failed creation
                log("Service failed to create {}".format(str(err)),
                    connect())

        elif desired == "Restart" and current != "Restarting" and current != "Stopped":
            try:
                self.restart_service(change["new_val"])
            except APIError as err:
                log("Service failed to restart {}".format(str(err)),
                    connect(host=os.getenv("RETHINK_HOST")))

        elif desired == "Stop" and current != "Restarting" and current != "Stopped":
            try:
                self.remove_service(change["new_val"])
            except (NotFound, APIError, KeyError) as err:
                log("Service failed to remove {}".format(str(err)),
                    connect(host=os.getenv("RETHINK_HOST")))

        elif desired == "" and change["new_val"]["State"] == "Stopped":
            if change["new_val"]["ServiceID"] in self.restarting_svc:
                self.restarting_svc.discard(change["new_val"]["ServiceID"])
                try:
                    self.create_service(change["new_val"])
                except APIError as err:
                    # notify failed creation
                    log("Service failed to create {}".format(str(err)),
                        connect(host=os.getenv("RETHINK_HOST")))
        else:
            log("Invalid transition for {sn} from {cs} to {ds}".format(sn=change["new_val"]['ServiceName'],
                                                                       cs=current,
                                                                       ds=desired),
                connect(host=os.getenv("RETHINK_HOST")))

    def establish_changefeed(self):
        """
        Establishes a changefeed to the Plugin table in the brain
        :ensures: self.plugin_changes will be a rethinkdb changefeed cursor
        :return None
        """
        changefeed_conn = connect(host=os.getenv("RETHINK_HOST"))
        self.plugin_changes = RPC.changes().run(changefeed_conn)

    def create_service(self, plugin_info, srv=None, img_name=None):
        """
        Creates a Docker service based on the plugin_info
        :param plugin_info: dict with structure that matches the Plugin table schema.
            assumes no ports are in conflict
        :return: None
        """
        res = add_ports(plugin_info, connect())
        if res["errors"] != 0:
            log("Duplicate ports detected", connect())
            return
        if img_name is None:
            img_name = self.get_image_from_plugin(plugin_info)
        if not srv:
            srv = self.generate_service_create(plugin_info)

        self.client.services.create(img_name, **srv)

    def remove_service(self, plugin_data):
        """
        Remove a service
        :param plugin_data:
        :return:
        """
        # get the service id
        srv = self.client.services.get(plugin_data["ServiceID"])
        # remove service
        srv.remove()
        # remove associated ports from brain
        remove_ports(plugin_data, connect(host=os.getenv("RETHINK_HOST")))

    def restart_service(self, plugin_data):
        self.restarting_svc.add(plugin_data["ServiceID"])
        # remove service
        self.remove_service(plugin_data)

    def get_image_from_plugin(self, plugin):
        """
        figures out which docker container to use
        based on the brain schema requested

        :param plugin: <dict> matching the plugins schema
        :return: <str> docker image name to use
        """
        image = "unknown"
        if plugin['OS'] == "nt":
            image = "interpreter-plugin-windows"
        else:
            if plugin['Extra']:
                selected_image = plugin.get("ImageName", "")
                if selected_image in self.image_names:
                    image = f"interpreter-plugin-{selected_image}"
                else:
                    image = "interpreter-plugin-extra"
            else:
                image = "interpreter-plugin"
        image_name = "{plugin_prefix}{image}:{tag}".format(plugin_prefix=PLUGIN_PREFIX, image=image, tag=self.tag)
        return image_name

    @staticmethod
    def get_endpoint_spec_from_plugin(plugin):
        """
        docker service create demands a EndpointSpec in a format
        mapping external port to internal port via a python <dict>

        ramrod brain passes them via two lists.

        This is a helper function to convert from brain to docker format.

        :param plugin: <dict> matching the plugins schema
        :return: <dict> matching to docker.EndpointSpec schema
        """
        docker_port_map = {}
        for i in range(len(plugin['ExternalPorts'])):
            e_port, e_proto = plugin['ExternalPorts'][i].split("/")
            i_port, i_proto = plugin['InternalPorts'][i].split("/")
            docker_port_map[int(e_port)] = (int(i_port), i_proto, "host")
        return EndpointSpec(**{"ports": docker_port_map})

    @staticmethod
    def ensure_populated_environment_variables(plugin):
        """
        Ensures that the environment includes required attirbutes


        DOES NOT UPDATE THE SOURCE PLUGIN CONTENT

        :param plugin: <dict> matching the plugins schema
        :return: <list>
        """


        # look if env already copied
        output_environment = []
        already_populated = bool(sum(["RETHINK" in item for item in plugin['Environment']]))
        if not already_populated:
            for key, value in environ.items():
                if key and value and "nt" not in plugin['OS']:
                    output_environment.append("{key}={value}".format(key=key, value=value))
            output_environment.append("PLUGIN={plugin_name}".format(plugin_name=plugin['Name']))
            output_environment.append("PLUGIN_NAME={plugin_name}".format(plugin_name=plugin['Name']))
            output_environment.append("PLUGIN_SERVICE_NAME={service_name}".format(service_name=plugin["ServiceName"]))
            output_environment.append("SERVICE_NAME={service_name}".format(service_name=plugin["ServiceName"]))
            output_environment.append("PLUGIN_RPC_ENTRY={rpc_entry}".format(rpc_entry=dumps(plugin)))
            # TODO: support multiple ports as env so plugin doens't need to pull their own record
            # TODO: consider implications for moving service to a new port, current requires a stop and new
            # TODO: do we support a plugin without a port?  Would throw exception on split
            first_port, first_protocol = plugin['InternalPorts'][0].split("/")
            if first_port and first_protocol:
                output_environment.append("PORT={port}".format(port=first_port))
            else:
                # TODO: better error condition than a hardcoded high port
                output_environment.append("PORT=0")
        # ##### START NETWORKING SHIM ########################################################
        # docker services  cannot NAT into the MCP overlay network while binding to host netowork
        # exposed address should be set by the advertise-addr, added as label by RUNONCE
        # docker swarm init --advertise-addr <THIS> --listen-addr
        rethink_host = environ.get("RETHINK_HOST", "rethinkdb")
        client = docker.from_env()  # already a staticmethod :-(
        manager_nodes = client.nodes.list(filters={"role": "manager"})
        for node in manager_nodes:
            rethink_host = node.attrs['Status']['Addr']  # this is set by --advertise-addr
        output_environment.append("RETHINK_HOST={rh}".format(rh=rethink_host))
        # ##### END NETWORKING SHIM ##########################################################

        # IMPORTANT!: Extend with user selected variables last so it overwrites parent ones on conflict
        output_environment.extend(plugin['Environment'])
        return output_environment

    @staticmethod
    def generate_constraints_list(plugin):
        """

        :param plugin: <dict> matching the plugins schema
        :return: <list>
        """
        from brain.controller import ADDRESS_KEY
        return [
            # node.labels.ip is enough to uniquely identify a node
            "node.labels.ip=={interface}".format(interface=plugin[ADDRESS_KEY]),
            # "node.labels.os=={os}".format(os=plugin["OS"])  # OS 'all' case on windows
        ]

    @staticmethod
    def generate_healthcheck(timeout=10**9 * 3,  # 3 seconds in nanoseconds ...docker
                             retries=3,
                             start_period=10**9 * 5  # 5 seconds in nanoseconds ... docker
                             ):
        """

        :param timeout: <int> docker requires to equal 0 or be greater than 10^6
        :param retries: <int>
        :param start_period: <int> docker requires to equal 0 or be greater than 10^6
        :return: <dict> matching
        """
        assert timeout>=10**6, "timeout must be > 1ms (why does docker want ns then?)"
        assert start_period >= 10 ** 6, "start_period must be > 1ms (why does docker want ns then?)"
        assert retries > 0, "retry at least once"
        assert retries < 10**6, "retry less than a million times"
        return Healthcheck(**{
            "timeout": timeout,
            "retries": retries,
            "start_period": start_period
        })

    @staticmethod
    def generate_networks(plugin=None, desired_networks=None):
        """
        Currently MCP deploy.sh hardcodes a single internal network (mcp).
        This may change in the future.
        If this changes,

        WARNING: Windows containers cannot be natted to the mcp network

        TODO: IF the setup changes where an administrator needs to change the netowrk name,
        this must change
        :param plugin: <dict> Plugin()
        :param desired_networks: <list> of <str>
        :return: <list> always a list of 1 item (mcp)
        """

        if not plugin:
            plugin = {}

        if not desired_networks:
            if "posix" in plugin.get("OS", "posix"):
                return ["mcp"]  # the default case today
            else:
                # TODO: Figure out why windows containers cannot use the mcp overlay network.
                return []

        # Future
        assert isinstance(desired_networks, list), "desired_network must be a list of strings"
        # TODO: if dpeloy.sh ever allows other networks, verify the desired_network is available
        return desired_networks


    @staticmethod
    def generate_restart_policy(condition="on-failure", max_attempts=3):
        """
        More sane defaults rather than the default docker
        :param condition: <str>
        :param max_attempts: <int>
        :return: <dict>
        """
        assert condition in {"none", "on-failure", "any"}
        assert max_attempts >= 0
        return RestartPolicy(**{
            "condition": condition,
            "max_attempts": max_attempts
        })

    @staticmethod
    def generate_service_mode(mode="replicated", replicas=1):
        """
        More sane defaults rather than the default docker

        :param mode:
        :param replicas:
        :return:
        """

        return ServiceMode(**{
            "mode": mode,
            "replicas": replicas
        })

    @staticmethod
    def generate_update_config(parallelism=0, delay=0):
        """

        :param parallelism:
        :param delay:
        :return:
        """
        assert parallelism >= 0, "parallelism should be 0 or greater"
        assert delay == 0 or delay > 10**6, "delay should be 0 or > 1ms"
        return UpdateConfig(**{
            "parallelism": parallelism,
            "delay": delay
        })

    @staticmethod
    def generate_mounts(mounts=None, skip_failure=False):
        """

        :param mounts:
        :param skip_failure:
        :return:
        """
        if not mounts:
            return []
        else:
            return mounts

    @staticmethod
    def generate_dns_config():
        """

        :return:
        """
        return DNSConfig()

    def generate_service_create(self, plugin):
        """

        :param plugin:
        :return:
        """
        return {
            "constraints": self.generate_constraints_list(plugin),
            "env": self.ensure_populated_environment_variables(plugin),
            "networks": self.generate_networks(plugin),
            "healthcheck": self.generate_healthcheck(),
            "restart_policy": self.generate_restart_policy(),
            "endpoint_spec": self.get_endpoint_spec_from_plugin(plugin),
            "mode": self.generate_service_mode(),
            "update_config": self.generate_update_config(),
            "tty": False,
            "open_stdin": False,
            "name": plugin["ServiceName"],
            "stop_grace_period": 3,
            "mounts": self.generate_mounts(),
            "dns_config": self.generate_dns_config()
        }
