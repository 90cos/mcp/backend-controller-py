from ..defaultlogger import DefaultLogger
from ...common import REGISTRY_PREFIX
from os import environ


class Jupyter(DefaultLogger):
    WANT = f"{REGISTRY_PREFIX}ramrod-notebook"

    def set_meta(self):
        self.meta['log_level'] = environ.get("LOGLEVEL", "DEBUG")
        self.meta['log_abunch'] = self.meta['log_level'] in {"", "DEBUG", "INFO", "WARN", "ERROR", "CRITICAL"}
        self.meta['log_medium'] = self.meta['log_level'] in {"WARN", "ERROR", "CRITICAL"}
        self.meta['log_little'] = self.meta['log_level'] in {"CRITICAL"}
        return

    def _filter_log(self, log):
        """
        strip out get/post logging if the loglevel is critical
        :param log:
        :return:
        """
        # TODO: This could stand for some optimization

        if log.startswith("[I"):
            if self.meta['log_abunch']:
                return log
            else:
                return ""

        if log.startswith("[W"):
            if self.meta['log_medium'] or self.meta['log_abunch']:
                return log
            else:
                return ""

        return log  # default to not touching the log because we don't know what it is
