from ..defaultlogger import DefaultLogger
from ...common import PLUGIN_PREFIX


class Plugin(DefaultLogger):
    WANT = f"{PLUGIN_PREFIX}interpreter-plugin"

    def set_meta(self):
        try:
            constraints = self.service.attrs['Spec']['TaskTemplate']['Placement']['Constraints']
            for constraint in constraints:
                if "node.labels.ip==" in constraint:
                    self.source_host = constraint.lstrip("node.labels.ip==")
        except KeyError:
            self.source_host = ""
