from ..defaultlogger import DefaultLogger
from ...common import REGISTRY_PREFIX
from os import environ


class Frontend(DefaultLogger):
    WANT = f"{REGISTRY_PREFIX}frontend-ui"

    def _filter_log(self, log):
        """
        strip out get/post logging if the loglevel is critical
        :param log:
        :return:
        """
        if environ.get("LOGLEVEL", "") == "CRITICAL":
            if '] "GET' in log or '] "POST' in log:
                return ""
        return log