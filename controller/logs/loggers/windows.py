from ..defaultlogger import DefaultLogger
from ...common import PLUGIN_PREFIX
from concurrent.futures import ThreadPoolExecutor
from time import sleep, time
from random import randint
import docker


class Windows(DefaultLogger):
    WANT = f"{PLUGIN_PREFIX}interpreter-plugin-windows"

    def set_meta(self):
        """
        see github.com/moby/moby/issues/30046
        windows does a bunch of caching before it dumps bytes to disk
        must periodically trigger a write to disk by directly calling service logs

        :return:
        """
        self.meta["executor"] = ThreadPoolExecutor(max_workers=2)
        self.meta["executor"].submit(self._trigger_windows_log_update, self.service.id)

    @staticmethod
    def _trigger_windows_log_update(service_id):
        """
        This function exists exclusively to trigger windows to write the bytes to disk
        once the bytes are written to disk, they will be sent up through the
        normal log follower and written into the database.
        """
        sleep(20)  # wait for service startup
        client = docker.from_env()
        service = client.services.get(service_id)
        since = int(time())
        while True:
            sleep(randint(7, 27))
            next_time = int(time())
            all_logs = list(service.logs(stdout=True, stderr=True, since=since))  # dereference the generator
            since = next_time
