from ..defaultlogger import DefaultLogger
from ...common import REGISTRY_PREFIX
from os import environ


class Brain(DefaultLogger):
    WANT = f"{REGISTRY_PREFIX}database-brain"

    def _filter_log(self, log):
        """
        strip out get/post logging if the loglevel is critical
        :param log:
        :return:
        """
        log = log.strip()
        if environ.get("LOGLEVEL", "") == "CRITICAL":
            if "DEBG 'rethink' stdout output:" in log or \
                    "Removing file /data/rethinkdb_data/" in log:
                return ""
        return log
