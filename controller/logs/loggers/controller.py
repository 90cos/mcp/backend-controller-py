from ..defaultlogger import DefaultLogger
from ...common import REGISTRY_PREFIX


class Controller(DefaultLogger):
    WANT = f"{REGISTRY_PREFIX}backend-controller"

    def _filter_log(self, log):
        """
        careful with recursive logging here

        :param log: <str>
        :return: <str>
        """
        log = log.strip(" ")  # controller logs blank lines sometimes

        return log
