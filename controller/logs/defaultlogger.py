from brain import connect
from os import environ
from time import time
from ..common.controller_common import log as send_log


class DefaultLogger(object):
    WANT = ""

    def __init__(self, what):
        self.service = what
        self.image_name = what.attrs['Spec']['TaskTemplate']['ContainerSpec']['Image']
        self.source_host = ""
        self.severity = 5
        self.meta = {}

    def set_meta(self):
        return

    def _log_generator(self, until=None):
        """

        :return: must return a generator object
        """
        return self.service.logs(follow=True, stdout=True, stderr=True)

    def follow_logs(self, until=None):
        self.set_meta()
        logs = self._log_generator()  # a generator
        conn = connect(host=environ.get("RETHINK_HOST", "localhost"))
        conn_time = time()
        for log in logs:
            log = log.decode(errors="ignore")
            log = self._filter_log(log)
            if log:
                if time() > conn_time + 45:
                    conn = connect(host=environ.get("RETHINK_HOST", "localhost"))
                    conn_time = time()

                send_log(log, conn,
                         service_name=self.service.name,
                         source_host=self.source_host,
                         severity=self.severity)

                # print(log)  # put it in the database
        return "done"

    def _filter_log(self, log):
        """
        Provides an opportunity for subclasses to filter or modify the log entry

        this function may
        1.  Return the same log entry it was given (will be inserted as is)
        2.  Modify and then return the log entry (modified log will be inserted)
        2.  Return a blank string (nothing will be sent to the database)

        :param log: <str>
        :return: <str>
        """
        # TODO: Consider a mutable object to not pass strings around as much?
        return log

