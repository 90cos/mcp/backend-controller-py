import sys
from inspect import getmembers, isclass
from queue import PriorityQueue

from .defaultlogger import DefaultLogger
from .loggers import *


class Logger(DefaultLogger):
    WANT = ""

    def __new__(cls, what):
        service = what
        image_name = what.attrs['Spec']['TaskTemplate']['ContainerSpec']['Image']
        wants = {DefaultLogger.WANT: DefaultLogger}
        modules = getmembers(sys.modules[__name__.strip(".defaultlogger")+".loggers"])

        for module in modules:
            if isclass(module[1]) and hasattr(module[1], "WANT"):
                if module[1].WANT not in wants:
                    wants[module[1].WANT] = module[1]
                else:
                    sys.stderr.write(
                        f"{module[0]} wants {module[1].WANTS}, but already known {wants}\n"
                    )

        pq = PriorityQueue()
        for want in wants:
            if image_name.startswith(want):
                pq.put(
                    (
                        1000-len(want),  # python is dumb and wants the lowest number as highest priority
                        wants[want]
                    )
                )
        who = pq.get(block=False)
        cls = who[1](service)
        return cls
