
from docker.errors import NotFound
from brain import connect
from sys import stderr
from ...common.controller_common import update_states_rpc


def container_events(docker_client, container_content):
    """
    monitoring container events, and updates plugin's
    states (DesiredState, and State)
    :param {dict} container_content:
    :return:
    """
    container_msg = "empty container return"
    conn = connect()
    try:
        container_object = docker_client.containers.get(container_content["Actor"]["ID"])
        # filtering plugins from mcp-core containers
        if container_object.name[:9] != "mcp-fleet" or "mcp-fleet_auxiliary-services" in container_object.name:
            service_name = container_object.name.split(".")[0]
            from pprint import pprint
            pprint(container_content)
            # new service or restart service (restart service step 2)
            if "exec_create" in container_content["Action"] and container_object.status == "running":
                # update RPC the State  is 'active', and DesiredState is ''
                stderr.write("start makring active\n")
                container_msg = update_states_rpc(conn=conn, service_name=service_name, plugins_state=ACTIVE)
                stderr.write("ended marking active\n")
                # self.health_check(container_object.name, container_object.attrs["State"]["Health"]["Status"])

            # remove service or restart service (restart service step 1)
            elif "exec_die" in container_content["Action"]:
                # Update RPC State to Stopped when docker service is told to stop from DSE
                stderr.write("start makring stop\n")
                container_msg = update_states_rpc(conn=conn, service_name=service_name, plugins_state=STOP)
                stderr.write("ended makring stop\n")

    except NotFound as err_msg:
        # log(err_msg, conn)
        print("container_events err_msg == {}".format(err_msg))
        container_msg = err_msg
    print("container_msg == {}".format(container_msg))
    return container_msg
