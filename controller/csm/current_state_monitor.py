"""
Monitor the docker events for plugin containers to
update brain of status changes and of health checks

Tail the docker service events from all current services.
Those evens should be both container events and service events.

For each of those:
if it is a service event, update the brain as needed.
Clear desired state, and set State on created state,
make sure to collect the docker id and put it in the brain.

success criteria:
when docker services change states, this code should
correctly update the brain in the event the service is
created, stopped, restarting.
"""
import os
import docker
from docker.errors import NotFound
from time import time, sleep
from sys import stderr, stdout

from brain import connect
from brain.environment import check_dev_env
from brain.static import ACTIVE, STOP
from ..common.controller_common import update_service_id_rpc, update_states_rpc, log, log_error
from .container.events import container_events


class CurrentStateMonitor(object):
    def __init__(self):
        self.client = docker.from_env()
        # self.conn = connect(host=os.getenv("RETHINK_HOST"))

    @staticmethod
    def health_check(container_name, health_status):
        """
        monitors container's health,
        if it's unhealthy the container will stop,
        if it's healthy the container will update the state to
        Activate if it's not already so
        :param container_name: {str} container's name
        :param health_status: {str} healthy or unhealthy
        :return: {str} container's healthy status
        """
        str_containers_health_status = ""
        try:
            if health_status == "healthy":
                # Just check or update plugin's DesiredState to 'Activate'
                str_containers_health_status = "\nCONTAINER'S HEALTH!\nContainer {} is {}\n".format(container_name,
                                                                                                    health_status)
            else:
                # Update RPC plugin's DesiredState to 'Stop'
                str_containers_health_status = "\nCONTAINER'S HEALTH!\nContainer {} is {}\n".format(container_name,
                                                                                                    health_status)
        except Exception as e:
            stderr.write(e)
            stderr.write("\n")
        stderr.write(str_containers_health_status)
        stderr.write("\n")
        return str_containers_health_status

    def service_events(self, service_content):
        #print(service_content)
        stderr.write("\n")
        if service_content["Action"] == "update":
            return self.service_events_update(service_content)
        elif service_content["Action"] == "remove":
            return self.service_events_remove(service_content)

    @staticmethod
    def service_events_remove(service_content):
        conn = connect()
        service_name = service_content['Actor']["Attributes"]["name"]
        if service_name.startswith("mcp-") or \
                service_name.startswith("Auxil"):
            pass  # because we don't log core events for now
            # TODO: perhaps do some logging.
        else:
            update_states_rpc(conn, service_name, STOP)
        return "removed {}".format(service_name)

    def service_events_update(self, service_content):
        """
        monitoring service events, and updates plugin's service id
        :param service_content: {dict} docker service events content
        :return:
        """
        service_msg = "empty service return"
        conn = connect()  # UPDATE conn to RETHINK_HOST
        try:
            max_wait = time() + 5
            service_object = self.client.services.get(service_content["Actor"]["ID"])
            service_tasks = service_object.tasks()
            while time() < max_wait or service_tasks[0]["Status"]["State"] != "running":
                service_object = self.client.services.get(service_content["Actor"]["ID"])
                service_tasks = service_object.tasks()
                sleep(0.5)
            # print("service_object attributes:\n{}\n".format(service_object.attrs))
            if service_content["Action"] == "update":  #TODO: No longer need this check
                # filtering plugins from mcp-core services

                # new service or restart service (restart service step 2)
                if service_object.name[:8] != "mcp-fleet" and \
                        service_tasks[0]["Status"]["State"] == "running" and \
                        service_tasks[0]["DesiredState"] == "running":

                    # Update RPC by filtering service name and update plugin's service id
                    service_msg = update_service_id_rpc(conn=conn,
                                                        service_name=service_object.name,
                                                        service_id=service_object.id)
                else:
                    error_msg = "{} service came online but mcp-core isn't tracking it.".format(service_object.name)
                    stderr.write(error_msg)
                    #log_error(conn, error_msg, service_msg)
                # remove service or restart service (restart service step 1)
                # elif service_object.name[:8] != "mcp-test" and service_tasks[0]["Status"]["State"] == "stop":
                    # Update RPC State to Stopped when docker service is told to stop from DSE
                    # pass

        except NotFound as err_msg:
            # log(err_msg, conn)
            stderr.write("service_events err_msg == {}\n".format(err_msg))
            # print(service_content)
            service_msg = err_msg
        #stderr.write("service_msg == {}\n".format(service_msg))
        return service_msg

    def run(self):
        """
        run's csm
        :return:
        """
        int_num_stack_cont = len(self.client.containers.list())
        # check if services and containers are running by docker events
        for dict_event in self.client.events(decode=True):
            try:
                if dict_event["Type"] == "container" and int_num_stack_cont != len(self.client.containers.list()):
                    try:
                        list_last_cont_up = self.client.containers.list(limit=1)
                        if check_dev_env():
                            print(
                                "NEW CONTAINER HAS BEEN ADDED!\n"
                                "LAST RUNNING CONTAINER:\n"
                                "NAME : {} \n"
                                "ID: {}: \n"
                                "IMAGE: {} \n".format(
                                    list_last_cont_up[0].name,
                                    list_last_cont_up[0].id,
                                    list_last_cont_up[0].image
                                )
                            )
                    except (docker.errors.NotFound, KeyError, IndexError, ValueError):
                        pass
                if dict_event["Type"] == "service":
                    if check_dev_env():
                        stderr.write(
                            "--Service--:\n"
                            "{}\n"
                            "{}\n".format(
                                dict_event["Actor"]["Attributes"]["name"],
                                dict_event
                            )
                        )
                    self.service_events(dict_event)
                if dict_event["Type"] == "container":
                    pass  # container events are inconsistent for remote hosts.
                    # stderr.write("--Container--:\n{}\n{}\n".format(dict_event["Actor"]["Attributes"]["name"], dict_event))
                    # container_events(self.client, dict_event)
            except KeyError:
                stderr.write("KeyError\n")
            # sleep(1)
